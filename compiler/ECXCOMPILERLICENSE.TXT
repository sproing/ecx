
/*********** THE ECX COMPILER LICENSE *******************************

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

o Redistributions must not be of commercial nature and must be free of charge.

o Copyright notices and this LICENSE may not be altered or removed from any
  source code.

o Redistributions in binary form must reproduce all copyright notices and
  this LICENSE in the documentation and/or other materials provided with
  the distribution.

o Redistributions of modified source code should not be misrepresented
  as the original and must clearly show who has modified it.

o Redistributions of modified binaries should not be misrepresented as the
  original and must use a different program name and a different archive name.

o Redistributions of modified binaries must in the documentation and/or other
  materials provided with the distribution, provide a brief explanation about
  how your work is based on this work.

This software is provided 'as-is', without any express or implied warranty.
In no event will the author(s) be held liable for any damages arising from
the use of this software. Use at your own risk.

************ END OF ECX COMPILER LICENSE ***************************/