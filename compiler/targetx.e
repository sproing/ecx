-> ECX/targetx.e Copyright (c) 2008 Leif Salomonsson

OPT MODULE, PREPROCESS, EXPORT, ___NOTARGETMOD

#ifdef _AMIGAOS
   OPT MODNAME = 'target_amigaos', AMIGAOS
   MODULE 'dos/dos',
          'exec/libraries',
          'workbench/startup',
          'exec/execbase',
          'dos/dosextens',
          'graphics/gfxbase',
          'intuition/intuitionbase'
STATIC ___m_default_1      = 'ecxmodules:amigaos/exec',
       ___m_default_2      = 'ecxmodules:amigaos/dos',
       ___m_default_3      = 'ecxmodules:amigaos/graphics',
       ___m_default_4      = 'ecxmodules:amigaos/intuition'
   #define __TARGET__ 'AmigaOS,68K'
   #define __AMIGAOS__ 1
   #define __M68K__ 1
#elifdef _MORPHOS
   OPT MODNAME = 'target_morphos', MORPHOS
   MODULE 'dos/dos',
          'exec/libraries',
          'workbench/startup',
          'exec/execbase',
          'dos/dosextens',
          'graphics/gfxbase',
          'intuition/intuitionbase'
STATIC ___m_default_1      = 'ecxmodules:morphos/exec',
       ___m_default_2      = 'ecxmodules:morphos/dos',
       ___m_default_3      = 'ecxmodules:morphos/graphics',
       ___m_default_4      = 'ecxmodules:morphos/intuition'
   #define __TARGET__ 'MorphOS,PPC'
   #define __MORPHOS__ 1
   #define __PPC__ 1
#elifdef _AMIGAOS4
   OPT MODNAME = 'target_amigaos4', AMIGAOS4
   MODULE 'dos/dos',
          'exec/libraries',
          'workbench/startup',
          'exec/execbase',
          'dos/dosextens',
          'graphics/gfxbase',
          'intuition/intuitionbase'
STATIC ___m_default_1      = 'ecxmodules:amigaos4/exec',
       ___m_default_2      = 'ecxmodules:amigaos4/dos',
       ___m_default_3      = 'ecxmodules:amigaos4/graphics',
       ___m_default_4      = 'ecxmodules:amigaos4/intuition'
   #define __TARGET__ 'AmigaOS4,PPC'
   #define __AMIGAOS4__ 1
   #define __PPC__ 1
#else
   #error 'unknown target'
#endif


CONST OLDFILE = MODE_OLDFILE,
      NEWFILE = MODE_NEWFILE,
      READWRITE = MODE_READWRITE




OBJECT ___internalglobs
      ___cleanupstack @  -4  /* ec comp */
      stdout @        -8  /* ec comp */
      conout @        -12 /* ec comp */
      stdrast @       -16 /* ec comp */
      ___memlist @       -20 /* ec comp */
      ___exit68k @       -24 /* 68k cleanupcode */
      ___clireturnval @  -28 /* ec */
      arg:PTR TO CHAR @  -32 /* ec comp */
      wbmessage:PTR TO wbarg @ -36 /* ec comp */
      execbase:PTR TO execbase @ -40 /* ec comp */
      dosbase:PTR TO doslibrary @ -44 /* ec comp */
      intuitionbase:PTR TO intuitionbase @ -48 /* ec comp */
      gfxbase:PTR TO gfxbase @   -52 /* ec comp */
      ___mathieeesingbasbase @   -56 /* ec comp */
      ___mathieeesingtransbase @ -60 /* ec comp */
      ___stackbottom @   -64 /* ec comp the startaddress of stack */->  AllocVec:ed
      ___codequal @      -68 /* [code:INT qualifier:INT] intui support */
      ___iaddress @      -72 /* intui support */
      ___code_return @   -76 /* 68k exceptions */
      ___stack_return @  -80 /* 68k exceptions */
      exception @     -84 /* ec comp */
      ___saveda5 @       -88 /* 68k exceptions */
      stdin @         -92 /* ec comp */
      exceptioninfo @ -96 /* ec comp */
      ___currchunk @     -100 /* FastNew() ("chopmem") */
      ___chunkleft @     -104 /* FastNew() ("chopleft") */
      ___cellsmem @      -108 /* ec cells */
      ___freecells @     -112 /* ec cells */
      ___chunksize @     -116 /* ec cells */

      /****** END EC GLOBALS ******/

      ___mempool @       -120 /* mempool for String()List(),DisposeLink() */
      ___utillib @       -124 /* CreativE "utilitybase" variable (NOT USED, KEEP NIL)*/
      ___cleanupframe @  -128 /* 1.6.0: 68k CleanUp() startup A5 */
      ___rwdata @        -132 -> V46: points to AllocVec:ed rwdata

      /* v45: own librarybase in library mode */
      librarybase:PTR TO lib @   -136

      -> powerpc globals
      ___randseed @      -140  -> Rnd()
      ___ppccleanup @    -144  -> adr of cleanupcode  used by CleanUp()
      ___exceptstruct @  -148  -> ppc exceptionhandling

      -> ppc list-quote function globals, to be removed eventually
      ___argsave0 @      -152  -> ppc quote+list functions..
      ___argsave1 @      -156
      ___argsave2 @      -160
      ___argsave3 @      -164
      ___argsave4 @      -168
      ___argsave5 @      -172
      ___argsave6 @      -176
      ___argsave7 @      -180

      ___initcode @   -184 -> 1.10.0
      ___stackswapstruct @   -188 -> 1.10.0 back (68k)
      ___rwdatasize @   -192 -> 2.0 size of rwdata, set by startup
      ___os4main @      -196 -> just a silly temp holder for main address in os4 startup

      -> ppc double float support
      ___mathieeedoubbasbase @   -200
      ___mathieeedoubtransbase @ -204

#ifdef _AMIGAOS4
      execiface @ -208
      dosiface @ -212
      gfxiface @ -216
      intuitioniface @ -220
      ___mathieeedoubbasiface @ -224
      ___mathieeedoubtransiface @ -228
      ___mathieeesingbasiface @ -232
      ___mathieeesingtransiface @ -236
#endif

      -> memtable for fastnew
      ___memtable[65]:ARRAY OF LONG @ -512 -> table compatible with EC. (260 bytes)

ENDOBJECT

