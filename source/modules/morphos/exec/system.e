OPT MODULE, EXPORT

-> exec/system.e (MorphOS)

MODULE 'utility/tagitem'

CONST SYSTEMINFOTYPE_SYSTEM               = $0
CONST SYSTEMINFOTYPE_MACHINE              = $1


CONST SYSTEMINFOTYPE_PPC_CPUVERSION       = $2
CONST SYSTEMINFOTYPE_PPC_CPUREVISION      = $3
CONST SYSTEMINFOTYPE_PPC_CPUCLOCK         = $4
CONST SYSTEMINFOTYPE_PPC_BUSCLOCK         = $5

CONST SYSTEMINFOTYPE_PPC_ICACHEL1SIZE     = $10
CONST SYSTEMINFOTYPE_PPC_ICACHEL1LINES    = $11
CONST SYSTEMINFOTYPE_PPC_ICACHEL1LINESIZE = $12
CONST SYSTEMINFOTYPE_PPC_DCACHEL1SIZE     = $13
CONST SYSTEMINFOTYPE_PPC_DCACHEL1LINES    = $14
CONST SYSTEMINFOTYPE_PPC_DCACHEL1LINESIZE = $15
CONST SYSTEMINFOTYPE_PPC_CACHEL1TYPE      = $16
CONST SYSTEMINFOTYPE_PPC_CACHEL1FLAGS     = $17

CONST SYSTEMINFOTYPE_PPC_ICACHEL2SIZE     = $20
CONST SYSTEMINFOTYPE_PPC_ICACHEL2LINES    = $21
CONST SYSTEMINFOTYPE_PPC_ICACHEL2LINESIZE = $22
CONST SYSTEMINFOTYPE_PPC_DCACHEL2SIZE     = $23
CONST SYSTEMINFOTYPE_PPC_DCACHEL2LINES    = $24
CONST SYSTEMINFOTYPE_PPC_DCACHEL2LINESIZE = $25
CONST SYSTEMINFOTYPE_PPC_CACHEL2TYPE      = $26
CONST SYSTEMINFOTYPE_PPC_CACHEL2FLAGS     = $27

CONST SYSTEMINFOTYPE_PPC_ICACHEL3SIZE     = $30
CONST SYSTEMINFOTYPE_PPC_ICACHEL3LINES    = $31
CONST SYSTEMINFOTYPE_PPC_ICACHEL3LINESIZE = $32
CONST SYSTEMINFOTYPE_PPC_DCACHEL3SIZE     = $33
CONST SYSTEMINFOTYPE_PPC_DCACHEL3LINES    = $34
CONST SYSTEMINFOTYPE_PPC_DCACHEL3LINESIZE = $35
CONST SYSTEMINFOTYPE_PPC_CACHEL3TYPE      = $36
CONST SYSTEMINFOTYPE_PPC_CACHEL3FLAGS     = $37

CONST SYSTEMINFOTYPE_PPC_TLBENTRIES       = $40
CONST SYSTEMINFOTYPE_PPC_TLBSETS          = $41

/*
 * PowerPC has a FPU
 */
CONST SYSTEMINFOTYPE_PPC_FPU              = $50
/*
 * PowerPC has an Altivec unit
 */
CONST SYSTEMINFOTYPE_PPC_ALTIVEC          = $51
/*
 * PowerPC has performance measurement cpu extension
 */
CONST SYSTEMINFOTYPE_PPC_PERFMONITOR      = $52
/*
 * PowerPC has datastream cpu extension.
 */
CONST SYSTEMINFOTYPE_PPC_DATASTREAM       = $53
/*
 * Reservation Size
 */
CONST SYSTEMINFOTYPE_PPC_RESERVATIONSIZE  = $60
/*
 * Bus Timer Ticks
 */
CONST SYSTEMINFOTYPE_PPC_BUSTICKS         = $61
/*
 * CPU Temperature in 8.24 fixedpoint, degrees celcius
 */
CONST SYSTEMINFOTYPE_PPC_CPUTEMP          = $62

/*
 * MMU Page Size
 */
CONST SYSTEMINFOTYPE_PAGESIZE             = $100

/*
 * Global Scheduler statistics (exec 50.42)
 */
CONST SYSTEMINFOTYPE_TBCLOCKFREQUENCY         = $200
CONST SYSTEMINFOTYPE_UPTIMETICKS              = $201
CONST SYSTEMINFOTYPE_LASTSECTICKS             = $202
CONST SYSTEMINFOTYPE_RECENTTICKS              = $203
CONST SYSTEMINFOTYPE_CPUTIME                  = $204
CONST SYSTEMINFOTYPE_LASTSECCPUTIME           = $205
CONST SYSTEMINFOTYPE_RECENTCPUTIME            = $206
CONST SYSTEMINFOTYPE_VOLUNTARYCSW             = $207
CONST SYSTEMINFOTYPE_INVOLUNTARYCSW           = $208
CONST SYSTEMINFOTYPE_LASTSECVOLUNTARYCSW      = $209
CONST SYSTEMINFOTYPE_LASTSECINVOLUNTARYCSW    = $20a
CONST SYSTEMINFOTYPE_LOADAVG1                 = $20b
CONST SYSTEMINFOTYPE_LOADAVG2                 = $20c
CONST SYSTEMINFOTYPE_LOADAVG3                 = $20d
/* Added in exec 50.45 */
CONST SYSTEMINFOTYPE_TASKSCREATED             = $20e
CONST SYSTEMINFOTYPE_TASKSFINISHED            = $20f
CONST SYSTEMINFOTYPE_LAUNCHTIMETICKS          = $210
CONST SYSTEMINFOTYPE_LAUNCHTIMETICKS1978      = $211
CONST SYSTEMINFOTYPE_TASKSRUNNING             = $212
CONST SYSTEMINFOTYPE_TASKSSLEEPING            = $213

CONST SYSTEMINFOTAG_DUMMY    = TAG_USER + $112000
CONST SYSTEMINFOTAG_CPUINDEX = SYSTEMINFOTAG_DUMMY + $0

