-> ECX:Source/MUI/Balancing.e

/*
** MUI-Demosource in E.
** Based on the C example 'Balancing.c' by Stefan Stuntz.
** Translated TO E by Sven Steiniger
**
** Sorry FOR some uppercase words in the comments. This IS because OF
** my AutoCase-dictionary
*/

OPT PREPROCESS


MODULE 'muimaster','libraries/mui',
       'intuition/classes','intuition/classusr',
       'dos/dos', 'utility/tagitem',
       'amigalib/boopsi'

PROC main() HANDLE
DEF app=NIL,window,sigs=0

  IF (muimasterbase:=OpenLibrary(MUIMASTER_NAME,MUIMASTER_VMIN))=NIL THEN
     Raise('Couldn\at open muimaster.library')

  #ifdef __AMIGAOS4__
   muimasteriface := GetInterface(muimasterbase, 'main', 1, NIL)
  #endif
        app:=ApplicationObject,
                MUIA_Application_Title      , 'BalanceDemo',
                MUIA_Application_Version    , '$VER: BalanceDemo 12.10 (21.11.95)',
                MUIA_Application_Copyright  , '�1995, Stefan Stuntz',
                MUIA_Application_Author     , 'Stefan Stuntz',
                MUIA_Application_Description, 'Show balancing groups',
                MUIA_Application_Base       , 'BALANCEDEMO',

                SubWindow, window:=WindowObject,
                        MUIA_Window_Title, 'Balancing Groups',
                        MUIA_Window_ID   , "BALA",
                        MUIA_Window_Width , MUIV_Window_Width_Screen(50),
                        MUIA_Window_Height, MUIV_Window_Height_Screen(50),

                        WindowContents, HGroup,

                                Child, VGroup, GroupFrame, MUIA_Weight, 15,
                                        Child, RectangleObject, TextFrame, MUIA_Weight,  50, End,
                                        Child, RectangleObject, TextFrame, MUIA_Weight, 100, End,
                                        Child, BalanceObject, End,
                                        Child, RectangleObject, TextFrame, MUIA_Weight, 200, End,
                                        End,

                                Child, BalanceObject, End,

                                Child, VGroup,
                                        Child, HGroup, GroupFrame,
                                                Child, RectangleObject, TextFrame, MUIA_ObjectID, 123, End,
                                                Child, BalanceObject, End,
                                                Child, RectangleObject, TextFrame, MUIA_ObjectID, 456, End,
                                                End,
                                        Child, HGroup, GroupFrame,
                                                Child, RectangleObject, TextFrame, End,
                                                Child, BalanceObject, End,
                                                Child, RectangleObject, TextFrame, End,
                                                Child, BalanceObject, End,
                                                Child, RectangleObject, TextFrame, End,
                                                Child, BalanceObject, End,
                                                Child, RectangleObject, TextFrame, End,
                                                Child, BalanceObject, End,
                                                Child, RectangleObject, TextFrame, End,
                                                End,
                                        Child, HGroup, GroupFrame,
                                                Child, HGroup,
                                                        Child, RectangleObject, TextFrame, End,
                                                        Child, BalanceObject, End,
                                                        Child, RectangleObject, TextFrame, End,
                                                        End,
                                                Child, BalanceObject, End,
                                                Child, HGroup,
                                                        Child, RectangleObject, TextFrame, End,
                                                        Child, BalanceObject, End,
                                                        Child, RectangleObject, TextFrame, End,
                                                        End,
                                                End,
                                        Child, HGroup, GroupFrame,
                                                Child, RectangleObject, TextFrame, MUIA_Weight,  50, End,
                                                Child, RectangleObject, TextFrame, MUIA_Weight, 100, End,
                                                Child, BalanceObject, End,
                                                Child, RectangleObject, TextFrame, MUIA_Weight, 200, End,
                                                End,
                                        Child, HGroup, GroupFrame,
                                                Child, SimpleButton('Also'),
                                                Child, BalanceObject, End,
                                                Child, SimpleButton('Try'),
                                                Child, BalanceObject, End,
                                                Child, SimpleButton('Sizing'),
                                                Child, BalanceObject, End,
                                                Child, SimpleButton('With'),
                                                Child, BalanceObject, End,
                                                Child, SimpleButton('Shift'),
                                                End,
                                        End,
                                End,
                        End,

                End;

        IF app=NIL THEN Raise('Failed TO create Application.')

        doMethodA(window,[MUIM_Notify,MUIA_Window_CloseRequest,MUI_TRUE,
                  app,2,MUIM_Application_ReturnID,MUIV_Application_ReturnID_Quit]);


/*
** This is the ideal input loop for an object oriented MUI application.
** Everything is encapsulated in classes, no return ids need to be used,
** we just check if the program shall terminate.
** Note that MUIM_Application_NewInput expects sigs to contain the result
** from Wait() (or 0). This makes the input loop significantly faster.
*/

        set(window,MUIA_Window_Open,MUI_TRUE);

        WHILE Not(doMethodA(app,[MUIM_Application_NewInput,{sigs}])=MUIV_Application_ReturnID_Quit)
          IF sigs THEN sigs:=Wait(sigs)
        ENDWHILE

        set(window,MUIA_Window_Open,FALSE);


/*
** Shut down...
*/

EXCEPT DO
   IF app THEN Mui_DisposeObject(app)
   #ifdef __AMIGAOS4__
   DropInterface(muimasteriface)
   #endif
   IF muimasterbase THEN CloseLibrary(muimasterbase)
   IF exception THEN WriteF('\s\n',exception)
ENDPROC

