
$VER: QuickPub documentation 1.0 (04.05.2004) Copyright � Damien Guichard

PURPOSE

  QuickPub opens a public screen in a simple double-click.
  Can be put in the WBStartup drawer.
  If you do you may wish to set tooltype BEHIND=TRUE.
  QuickPub.mos is the MorphOS exe.
  Amiga-E source included.

REQUIREMENTS

  OS37+, reqtools.library

  Can be started from either Workbench or Shell.
  The file QuickPub.info must not be deleted,
  it contains the tooltypes used to open the public screen.
  The PUBNAME tooltype is mandatory.

TOOLTYPES

  Following tooltypes are understood:

  COMMAND

  DEPTH
  DISPLAYID
  PUBNAME
  TITLE

  LIKEWORKBENCH

  LEFT
  TOP
  WIDTH
  HEIGHT

  DETAILPEN
  BLOCKPEN
  SYSFONT

  OVERSCAN

  SHOWTITLE
  BEHIND
  QUIET
  AUTOSCROLL
  FULLPALETTE
  DRAGGABLE
  SHAREPENS
  INTERLEAVED
  MINIMIZEISG

  They can be set to TRUE (or YES), to FALSE (or NO), to a string,
  and to a decimal/hex/bin value.

  See Amiga Includes "intuition/screens.h" for more info.

ACKNOWLEDGEMENTS

  Thanks to:

  Wouter van Oortmerssen
    for the innovative E language.
    and the lighting fast EC compiler.

    wouter.fov120.com/e/"

  Leif Salomonsson
    for the ECX compiler.
    register ECX, you will not regret.

    home.swipnet.se/blubbe/ECX"

  Nico Fran�ois
     for the Reqtools library.


